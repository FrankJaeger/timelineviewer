//
//  StringExtension.swift
//  TimelineViewer
//
//  Created by Przemek on 16.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}