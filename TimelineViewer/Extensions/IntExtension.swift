//
//  IntExtension.swift
//  TimelineViewer
//
//  Created by Przemek on 21.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import Foundation
import UIKit

extension Int {
    var cgfloatValue: CGFloat {
        return CGFloat(self)
    }
}