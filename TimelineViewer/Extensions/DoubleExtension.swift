//
//  DoubleExtension.swift
//  TimelineViewer
//
//  Created by Przemek on 24.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    var cgfloatValue: CGFloat {
        return CGFloat(self)
    }
}