//
//  FavPosts.swift
//  
//
//  Created by Przemek on 28.07.2015.
//
//

import Foundation
import CoreData

class FavPosts: NSManagedObject {
    
    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?){
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }
    
    @NSManaged var authorName: String?
    @NSManaged var authorId: String?
    @NSManaged var id: String
    @NSManaged var createdTime: String?
    @NSManaged var message: String?
    @NSManaged var desc: String?
    @NSManaged var authorProfileImage: NSData?
    @NSManaged var authorCoverImage: NSData?
    @NSManaged var postImage: NSData?
    @NSManaged var caption: String?
    @NSManaged var userid: String
}