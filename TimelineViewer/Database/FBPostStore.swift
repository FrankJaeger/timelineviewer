//
//  FBPostStore.swift
//  TimelineViewer
//
//  Created by Przemek on 28.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class FBPostStore {
    
    static private let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    private init() {}
    
    static func savePost(post: FBPost) -> Bool {
        let entityDescription = NSEntityDescription.entityForName("FavPosts", inManagedObjectContext: managedObjectContext!)
        
        var error: NSError?
        var isDuplicate: Bool = false
        
        let request = NSFetchRequest()
        request.entity = entityDescription
        
        var objects = managedObjectContext?.executeFetchRequest(request, error: &error)
        
        if let results = objects {
            if results.count > 0 {
                for match in results {
                    if let id = match.valueForKey("id") as? String {
                        if (id == post.id) {
                            isDuplicate = true
                        }
                    }
                }
            } else {
                //NSLog("A problem during database reading occured: \(error?.localizedDescription)")
            }
        }

        let favPost: FavPosts = FavPosts(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        
        favPost.userid = FBConnection.sharedInstance.getCurrentAccessToken()!.userID
        favPost.authorId = post.authorId
        favPost.authorName = post.authorName
        favPost.caption = post.caption
        favPost.createdTime = post.friendlyCreateTime
        favPost.desc = post.desc
        favPost.id = post.id
        favPost.message = post.message
        if let aCoverImage = post.authorCoverImage {
            favPost.authorCoverImage = UIImageJPEGRepresentation(aCoverImage, 1)
        }
        if let aProfileImage = post.authorProfileImage {
            favPost.authorProfileImage = UIImageJPEGRepresentation(aProfileImage, 1)
        }
        if let pImage = post.postImage {
            favPost.postImage = UIImageJPEGRepresentation(pImage, 1)
        }
        
        if (!isDuplicate) {
            managedObjectContext?.save(&error)
            
            if let er = error {
                NSLog("Failed to save post as favorite! \(er.localizedFailureReason)")
            } else {
                NSLog("Post saved!")
                return true
            }
        } else {
            NSLog("Tried to add duplicated instance of post to Core Data.")
        }

        return false
    }
    
    static func getFavPosts() -> [FBPost]? {
        let entityDescription = NSEntityDescription.entityForName("FavPosts", inManagedObjectContext: managedObjectContext!)
        let request = NSFetchRequest()
        
        request.entity = entityDescription
        
        var error: NSError?
        var objects = managedObjectContext?.executeFetchRequest(request, error: &error)
        var Posts = [FBPost]()
        
        if let results = objects {
            if results.count > 0 {
                for match in results {
                    let post = FBPost()
                    
                    if ((match.valueForKey("userid") as? String) == FBConnection.sharedInstance.getCurrentAccessToken()?.userID) {
                        post.storedAuthorId = match.valueForKey("authorId") as? String
                        post.storedAuthorName = match.valueForKey("authorName") as? String
                        post.caption = match.valueForKey("caption") as? String
                        post.storedFriendlyCreatedTime = match.valueForKey("createdTime") as? String
                        post.desc = match.valueForKey("desc") as? String
                        post.id = match.valueForKey("id") as! String
                        post.message = match.valueForKey("message") as? String
                        if let coverData = match.valueForKey("authorCoverImage") as? NSData {
                            post.authorCoverImage = UIImage(data: coverData)
                        }
                        if let profileData = match.valueForKey("authorProfileImage") as? NSData {
                            post.authorProfileImage = UIImage(data: profileData)
                        }
                        if let postData = match.valueForKey("postImage") as? NSData {
                            post.postImage = UIImage(data: postData)
                        }
                        Posts.append(post)
                    }
                }
            } else {
                return nil
            }
        }
        return Posts
    }
    
    static func removePost(id: String) {
        let entityDescription = NSEntityDescription.entityForName("FavPosts", inManagedObjectContext: managedObjectContext!)
        let request = NSFetchRequest()
        
        request.entity = entityDescription
        
        var error: NSError?
        var objects = managedObjectContext?.executeFetchRequest(request, error: &error)
        var Posts = [FBPost]()
        
        if let results = objects {
            if results.count > 0 {
                for match in results {
                    if let ID = match.valueForKey("id") as? String {
                        if (ID == id && match.valueForKey("userid") as? String == FBConnection.sharedInstance.getCurrentAccessToken()?.userID) {
                            managedObjectContext?.deleteObject(match as! NSManagedObject)
                            NSLog("Post of id \(id) deleted")
                        }
                    }
                }
                managedObjectContext?.save(nil)
            } else {
                NSLog("Could not delete post of id \(id).")
            }
        }
    }
}