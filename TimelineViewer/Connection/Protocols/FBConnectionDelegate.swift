//
//  FBConnectionDelegate.swift
//  TimelineViewer
//
//  Created by Przemek on 21.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import Foundation
import FBSDKLoginKit

@objc protocol FBConnectionDelegate {
    optional func fbConnectionAuthenticationSuccessful(result: FBSDKLoginManagerLoginResult?)
    optional func fbConnectionAuthenticationCancelled()
    optional func fbConnectionAuthenticationError(error: NSError)
    
    optional func fbDidFetchFeedJSON(feed: AnyObject, nextKey: String?)
    optional func fbFetchErrorOccured(error: NSError)
}