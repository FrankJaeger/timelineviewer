//
//  FBConnection.swift
//  TimelineViewer
//
//  Created by Przemek on 16.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import SwiftyJSON
import Alamofire

enum fbProfileImageSize: String {
    case Normal = "normal"
    case Large = "large"
    case Album = "album"
}

let FBConnectionErrorDomain = "pl.foluszczyk.TimelineViewer.FBConnection"

class FBConnection {
    
    static let sharedInstance = FBConnection()
    static let AccessTokenDidChangeNotification = FBSDKAccessTokenDidChangeNotification
    static let AccessTokenDidChangeUserID = FBSDKAccessTokenDidChangeUserID
    
    private let loginManager = FBSDKLoginManager()
    
    var isUserLoggedIn: Bool {
        return FBSDKAccessToken.currentAccessToken() != nil
    }
    
    private init() {}
    
    func invokeLoginDialog(sender: FBConnectionDelegate? = nil) {
        let permissions = [
            "user_posts"
        ]
        
        let requestHandler: FBSDKLoginManagerRequestTokenHandler = { (result, error) -> Void in
            if (error != nil) {
                sender?.fbConnectionAuthenticationError?(error)
            } else if (result.isCancelled) {
                sender?.fbConnectionAuthenticationCancelled?()
            } else {
                sender?.fbConnectionAuthenticationSuccessful?(result)
            }
        }
        
        loginManager.logInWithReadPermissions(permissions, handler: requestHandler)
    }
    
    func logout() {
        FBSDKUtility.cancelPreviousPerformRequestsWithTarget(self)
        loginManager.logOut()
    }
    
    func getCurrentAccessToken() -> FBSDKAccessToken? {
        return FBSDKAccessToken.currentAccessToken()
    }
    
    func getPosts(sender: FBConnectionDelegate?, key: String? = nil) {
        var graphPath = ""
        
        if (key != nil) {
            graphPath = "/me/\(key!)"
        } else {
            graphPath = "/me/feed?fields=created_time,description,from,full_picture,id,message,link,name,source,type,attachments,caption&limit=10"
        }
        
        let graphRequest = FBSDKGraphRequest(
            graphPath: graphPath,
            parameters: nil,
            HTTPMethod: "GET")
        
        if let token = FBSDKAccessToken.currentAccessToken() {
            graphRequest.setValue(token.tokenString, forKey: "tokenString")
        }
        
        if (getCurrentAccessToken() != nil) {
            graphRequest.startWithCompletionHandler { (fbGraphConnection, result, error) -> Void in
                if (error == nil) {
                    let jResult = JSON(result)
                    var nextKeyParsed = ""
                    
                    if let nextKey = jResult["paging"]["next"].string {
                        let range = nextKey.rangeOfString("feed?")
                        
                        nextKeyParsed = nextKey.substringFromIndex(range!.startIndex)
                    }
                    
                    if let resultDict = result as? NSDictionary {
                        if let data: AnyObject = resultDict["data"] {
                            sender?.fbDidFetchFeedJSON?(data, nextKey: nextKeyParsed)
                        } else {
                            let er = NSError(domain: FBConnectionErrorDomain, code: 0, userInfo: nil)
                            sender?.fbFetchErrorOccured?(er)
                        }
                    } else {
                        let er = NSError(domain: FBConnectionErrorDomain, code: 1, userInfo: nil)
                        sender?.fbFetchErrorOccured?(er)
                    }
                } else {
                    sender?.fbFetchErrorOccured?(error)
                    NSLog("An error occured during feed fetch: %@", error)
                }
            }
        }
    }
}