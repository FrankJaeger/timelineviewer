//
//  MenuViewController.swift
//  TimelineViewer
//
//  Created by Przemek on 21.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import UIKit

enum viewName: Int {
    case Posts = 0, Account, Favorites, AboutUs, Logout
}

enum animationType {
    case Returning, Changing, AppLaunching
}

class MenuViewController: UIViewController {
    
    let menuItemCenterX = 229
    private let menuTopOffset = 170
    private let menuItemsGap = 50
    private let inMenuViewScale = 0.68.cgfloatValue
    private let inMenuViewCenterXDelta = MainContainerViewController.sharedInstance.view.bounds.width * 0.6

    @IBOutlet var menuItems: Array<UIButton>!
    @IBOutlet weak var menuInvokeButton: UIButton!
    @IBOutlet weak var menuCloseButton: UIButton!
    @IBOutlet weak var loadMore: UIButton!
    
    var lastChoose: (Int, UIViewController?) = (0, nil)
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    static func menuSlideIn() {
        let animationBlock: (UIViewController, UIViewController, (UIImageView!, CGFloat), () -> Void) -> Void = {
            (currentlyActiveVc, addedVc, background, completion) -> Void in
            let fromVc: UIViewController = currentlyActiveVc
            let toVc: MenuViewController = addedVc as! MenuViewController
            let blankView = UIView(frame: fromVc.view.frame)
            let tapRecognizer = UITapGestureRecognizer(target: toVc, action: Selector("menuCurrentViewTapped:"))
            let panRecognizer = UIPanGestureRecognizer(target: toVc, action: Selector("menuCurrentViewPanned:"))
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            let effectView = UIVisualEffectView(effect: blurEffect)
            
            effectView.alpha = 0
            effectView.frame = background.0.frame
            background.0.addSubview(effectView)
            
            blankView.addGestureRecognizer(tapRecognizer)
            blankView.addGestureRecognizer(panRecognizer)
            fromVc.view.addSubview(blankView)
            
            toVc.view.center.x -= MainContainerViewController.sharedInstance.view.bounds.width
            for (idx, button) in enumerate(toVc.menuItems.filter {$0 != toVc.menuItems[toVc.lastChoose.0]}) {
                button.center.x = toVc.menuItemCenterX.cgfloatValue - MainContainerViewController.sharedInstance.view.bounds.width
                UIView.animateWithDuration(0.4, delay: Double(idx) * 0.11, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                    button.center.x = toVc.menuItemCenterX.cgfloatValue
                    }, completion: nil)
            }
            
            UIView.animateWithDuration(0.8, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                fromVc.view.center.x = MainContainerViewController.sharedInstance.view.center.x + toVc.inMenuViewCenterXDelta
                fromVc.view.transform = CGAffineTransformMakeScale(toVc.inMenuViewScale, toVc.inMenuViewScale)
                }, completion: nil)
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                toVc.view.center.x += MainContainerViewController.sharedInstance.view.bounds.width * 0.75
                }, completion: { (completed) -> Void in
                    
            })
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                background.0.center.x += background.1 / 2
                effectView.alpha = 0.7
            })
        }
        
        if (MainContainerViewController.sharedInstance.changeViewTo("MenuViewController", withAnimation: animationBlock, releaseOld: false) == nil) {
            MainContainerViewController.sharedInstance.changeViewTo(MenuViewController(nibName: "MenuViewController", bundle: nil), named: "MenuViewController", withAnimation: animationBlock, releaseOld: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UI_USER_INTERFACE_IDIOM() == .Pad) {
            let revealController = self.revealViewController()
            
            menuInvokeButton.addTarget(self, action: Selector("iPadMenuToggle"), forControlEvents: UIControlEvents.TouchUpInside)
            menuCloseButton.addTarget(self, action: Selector("iPadMenuToggle"), forControlEvents: UIControlEvents.TouchUpInside)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
    }
    
    override func viewDidLayoutSubviews() {
        if (UI_USER_INTERFACE_IDIOM() != .Pad) {
            let itemsToDisplay = menuItems.filter {$0 != self.menuItems[self.lastChoose.0]}
            
            menuItems[lastChoose.0].hidden = true
            
            for (idx, menuItem) in enumerate(itemsToDisplay) {
                let gap = (idx == 0) ? menuTopOffset.cgfloatValue : itemsToDisplay[idx - 1].center.y + menuItemsGap.cgfloatValue
                
                menuItem.center.y = gap
                menuItem.hidden = false
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func iPadMenuToggle() {
        let revealController = self.revealViewController()
        
        if (revealController.frontViewPosition == .Right) {
            revealController.setFrontViewPosition(.RightMost, animated: true)
            
            self.menuInvokeButton.userInteractionEnabled = false
            self.menuCloseButton.userInteractionEnabled = true
            
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.menuInvokeButton.alpha = 0
                self.menuCloseButton.alpha = 1
            })
        } else {
            revealController.setFrontViewPosition(.Right, animated: true)
            
            self.menuInvokeButton.userInteractionEnabled = true
            self.menuCloseButton.userInteractionEnabled = false
            
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.menuInvokeButton.alpha = 1
                self.menuCloseButton.alpha = 0
            })
        }
    }
    
    // MARK: - Items displaying methods
    
    func menuDisplayPosts(animation: animationType) -> UIViewController? {
        let returning = (animation == .Returning) ? true : false
        
        lastChoose.0 = viewName.Posts.rawValue
        
        if let vC = MainContainerViewController.sharedInstance.changeViewTo("PostsViewController", withAnimation: menuGetViewAnimationBlock(animation), releaseOld: true, returning: returning) {
            lastChoose.1 = vC
            return vC
        } else {
            let vC = MainContainerViewController.sharedInstance.changeViewTo(PostsViewController(nibName: "PostsViewController", bundle: nil), named: "PostsViewController", withAnimation: menuGetViewAnimationBlock(animation), releaseOld: true)
            lastChoose.1 = vC
            return vC
        }
    }
    
    func menuDisplayAccount(animation: animationType) -> UIViewController? {
        let returning = (animation == .Returning) ? true : false
        
        lastChoose.0 = viewName.Account.rawValue
        
        if let vC = MainContainerViewController.sharedInstance.changeViewTo("AccountViewController", withAnimation: menuGetViewAnimationBlock(animation), releaseOld: true, returning: returning) {
            lastChoose.1 = vC
            return vC
        } else {
            let vC = MainContainerViewController.sharedInstance.changeViewTo(AccountViewController(nibName: "AccountViewController", bundle: nil), named: "AccountViewController", withAnimation: menuGetViewAnimationBlock(animation), releaseOld: true, returning: returning)
            lastChoose.1 = vC
            return vC
        }
    }
    
    func menuDisplayFavorites(animation: animationType) -> UIViewController? {
        let returning = (animation == .Returning) ? true : false
        
        lastChoose.0 = viewName.Favorites.rawValue
        
        if let vC = MainContainerViewController.sharedInstance.changeViewTo("FavoritesViewController", withAnimation: menuGetViewAnimationBlock(animation), releaseOld: true, returning: returning) {
            lastChoose.1 = vC
            return vC
        } else {
            let vC = MainContainerViewController.sharedInstance.changeViewTo(FavoritesViewController(nibName: "FavoritesViewController", bundle: nil), named: "FavoritesViewController", withAnimation: menuGetViewAnimationBlock(animation), releaseOld: true, returning: returning)
            lastChoose.1 = vC
            return vC
        }
    }
    
    func menuDisplayAboutUs(animation: animationType) -> UIViewController? {
        let returning = (animation == .Returning) ? true : false
        
        lastChoose.0 = viewName.AboutUs.rawValue
        
        if let vC = MainContainerViewController.sharedInstance.changeViewTo("AboutUsViewController", withAnimation: menuGetViewAnimationBlock(animation), releaseOld: true, returning: returning) {
            lastChoose.1 = vC
            return vC
        } else {
            let vC = MainContainerViewController.sharedInstance.changeViewTo(AboutUsViewController(nibName: "AboutUsViewController", bundle: nil), named: "AboutUsViewController", withAnimation: menuGetViewAnimationBlock(animation), releaseOld: true, returning: returning)
            lastChoose.1 = vC
            return vC
        }
    }
    
    func menuEaseOutCurrentView(completion: () -> Void) {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            UIView.animateWithDuration(0.2, animations: { () -> Void in
                self.lastChoose.1?.view.center.x += MainContainerViewController.sharedInstance.view.bounds.width / 2
                self.lastChoose.1?.view.transform = CGAffineTransformMakeScale(0.2, 0.2)
            })
        })
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), { () -> Void in
            completion()
        })
    }
    
    func menuSlideOut() {
        for (idx, button) in enumerate(self.menuItems.filter {$0 != self.menuItems[self.lastChoose.0]}) {
            UIView.animateWithDuration(0.4, delay: Double(idx) * 0.11, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: nil, animations: {
                () -> Void in
                button.center.x -= MainContainerViewController.sharedInstance.view.bounds.width
            }, completion: nil)
        }
    }
    
    func menuShowChoosedItem(choosedItem: viewName, withAnimation animation: animationType = .Changing) {
        if (UI_USER_INTERFACE_IDIOM() != .Pad) {
           menuSlideOut()
        } else {
            loadMore.alpha = 0
            loadMore.enabled = false
        }
        switch (choosedItem) {
        case .Posts:
            if (UI_USER_INTERFACE_IDIOM() == .Pad) {
                let splitController = MainSplitViewController()
                let postsController = PostsViewController(nibName: "PostsViewController", bundle: nil)
                
                postsController.splitVcDelegate = splitController
                
                splitController.viewControllers = [
                    postsController,
                    SinglePostViewController(nibName: "SinglePostViewController", bundle: nil)
                ]
                
                self.revealViewController().setFrontViewController(splitController, animated: true)
            } else {
                if (animation != .Returning) {
                    self.menuEaseOutCurrentView({
                        self.lastChoose.1?.view = nil
                        self.menuDisplayPosts(animation)
                    })
                } else {
                    self.menuDisplayPosts(animation)
                }
            }
        case .Account:
            if (UI_USER_INTERFACE_IDIOM() == .Pad) {
                self.revealViewController().setFrontViewController(AccountViewController(nibName: "AccountViewController", bundle: nil), animated: true)
            } else {
                if (animation != .Returning) {
                    self.menuEaseOutCurrentView({
                        self.lastChoose.1?.view = nil
                        self.menuDisplayAccount(animation)
                    })
                } else {
                    self.menuDisplayAccount(animation)
                }
            }
        case .Favorites:
            if (UI_USER_INTERFACE_IDIOM() == .Pad) {
                let splitController = MainSplitViewController()
                let favController = FavoritesViewController(nibName: "FavoritesViewController", bundle: nil)
                
                favController.splitVcDelegate = splitController
                
                splitController.viewControllers = [
                    favController,
                    SinglePostViewController(nibName: "SinglePostViewController", bundle: nil)
                ]
                
                self.revealViewController().setFrontViewController(splitController, animated: true)
            } else {
                if (animation != .Returning) {
                    self.menuEaseOutCurrentView({
                        self.lastChoose.1?.view = nil
                        self.menuDisplayFavorites(animation)
                    })
                } else {
                    self.menuDisplayFavorites(animation)
                }
            }
        case .AboutUs:
            if (UI_USER_INTERFACE_IDIOM() == .Pad) {
                self.revealViewController().setFrontViewController(AboutUsViewController(nibName: "AboutUsViewController", bundle: nil), animated: true)
            } else {
                if (animation != .Returning) {
                    self.menuEaseOutCurrentView({
                        self.lastChoose.1?.view = nil
                        self.menuDisplayAboutUs(animation)
                    })
                } else {
                    self.menuDisplayAboutUs(animation)
                }
            }
        case .Logout:
            FBConnection.sharedInstance.logout()
        default:
            break
        }
    }
    
    func menuGetViewAnimationBlock(type: animationType) -> ((UIViewController, UIViewController, (UIImageView!, CGFloat), () -> Void) -> Void) {
        let delay = (type == .AppLaunching) ? 0.2 : 0
        let viewAnimationBlock: (UIViewController, UIViewController, (UIImageView!, CGFloat), () -> Void) -> Void = { (currentlyActiveVc, addedVc, background, completion) -> Void in
            let newVc = addedVc as UIViewController

            if (type != .Returning) {
                newVc.view.center.x += MainContainerViewController.sharedInstance.view.bounds.width
                newVc.view.transform = CGAffineTransformMakeScale(0.5, 0.5)
            }

            UIView.animateWithDuration(0.6, delay: delay, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.1, options: nil, animations: { () -> Void in
                newVc.view.center.x = MainContainerViewController.sharedInstance.view.center.x
                newVc.view.transform = CGAffineTransformMakeScale(1, 1)
                
                }, completion: { (completed) -> Void in
                    completion()
            })
        }
        
        return viewAnimationBlock
    }
    
    // MARK: - Interface Events
    
    func menuCurrentViewTapped(recognizer: UITapGestureRecognizer) {
        let currentView = recognizer.view?.superview
        
        recognizer.view?.removeFromSuperview()
        menuShowChoosedItem(viewName(rawValue: lastChoose.0)!, withAnimation: .Returning)
        recognizer.view?.removeGestureRecognizer(recognizer)
    }
    
    func menuCurrentViewPanned(recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translationInView(MainContainerViewController.sharedInstance.view)
        let viewsGap = (recognizer.view!.superview!.center.x - MainContainerViewController.sharedInstance.view.center.x) / 100
        let maxGap = ((inMenuViewCenterXDelta) / (1 - inMenuViewScale)) / 100
        let scale = 1 - viewsGap / maxGap
        
        self.view.userInteractionEnabled = false
        if (translation.x + recognizer.view!.superview!.center.x > MainContainerViewController.sharedInstance.view.center.x - 1) {
            UIView.animateWithDuration(0.2, animations: { () -> Void in
                recognizer.view?.superview?.center.x += translation.x
                //self.view.center.x += translation.x
                recognizer.view?.superview?.transform = CGAffineTransformMakeScale(scale, scale)
                }, completion: nil)
            for (idx, button) in enumerate(self.menuItems.filter {$0 != self.menuItems[self.lastChoose.0]}) {
                UIView.animateWithDuration(0.5, delay: Double(idx) * 0.04, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                    let bCenterX = (viewsGap) * self.menuItemCenterX.cgfloatValue - self.menuItemCenterX.cgfloatValue
                    
                    if (bCenterX < self.menuItemCenterX.cgfloatValue) {
                        button.center.x = bCenterX
                    }
                    }, completion: nil)
            }
        }
        if (recognizer.state == .Ended) {
            self.view.userInteractionEnabled = true
            if (viewsGap > MainContainerViewController.sharedInstance.view.bounds.width / 300) {
                UIView.animateWithDuration(0.8, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                    recognizer.view!.superview!.center.x = MainContainerViewController.sharedInstance.view.center.x + self.inMenuViewCenterXDelta
                    recognizer.view!.superview!.transform = CGAffineTransformMakeScale(self.inMenuViewScale, self.inMenuViewScale)
                    
                    for (idx, button) in enumerate(self.menuItems.filter {$0 != self.menuItems[self.lastChoose.0]}) {
                        UIView.animateWithDuration(0.2, delay: Double(idx) * 0.11, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.3, options: nil, animations: { () -> Void in
                            button.center.x = self.menuItemCenterX.cgfloatValue
                            }, completion: nil)
                    }
                    }, completion: nil)
            } else {
                recognizer.view?.removeFromSuperview()
                menuShowChoosedItem(viewName(rawValue: lastChoose.0)!, withAnimation: .Returning)
                recognizer.view?.removeGestureRecognizer(recognizer)
            }
        }
        
        recognizer.setTranslation(CGPoint(x: 0, y: 0), inView: MainContainerViewController.sharedInstance.view)
    }
    
    @IBAction func menuItemSelected(sender: UIButton) {
        sender.transform = CGAffineTransformMakeScale(1.1, 1.1)
        
        UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
            sender.transform = CGAffineTransformMakeScale(1, 1)
        }) { (completed) -> Void in
        
        }
        
        if (sender == self.menuItems[sender.tag]) {
            menuShowChoosedItem(viewName(rawValue: sender.tag)!)
        }
    }
}