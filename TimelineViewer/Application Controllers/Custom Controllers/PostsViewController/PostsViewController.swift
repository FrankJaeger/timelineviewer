//
//  PostsViewController.swift
//  TimelineViewer
//
//  Created by Przemek on 22.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import UIKit
import SwiftyJSON
import FBSDKCoreKit
import Alamofire

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FBConnectionDelegate {
    
    private let imageCache = NSCache()
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var postsTableView: UITableView!
    @IBOutlet weak var postsDownloadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadMoreButton: UIButton!
    @IBOutlet weak var loadMoreButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var refreshProgressBar: UIProgressView!
    @IBOutlet weak var warningMessage: UILabel!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var splitVcDelegate: UISplitViewController? = nil
    
    private var userid: String? = nil
    private var hasNewPosts: Bool = true
    private var isRefreshing: Bool = false
    private var postsTableViewOffsetY: CGFloat = 0
    private var postsTableViewLastOffsetY: CGFloat = 0
    private var loadMoreKey: String = ""
    private var fbPosts = [FBPost]()
    private var postsCount: Int {
        return fbPosts.count
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let postCellNib = UINib(nibName: "SinglePostTableViewCell", bundle: nil)
        
        if (UI_USER_INTERFACE_IDIOM() == .Pad) {
            menuButton.image = nil
            menuButton.enabled = false
        }
        
        loadMoreButtonHeight.constant = 0
        navBar.topItem?.title = "all_posts".localized
        navBar.titleTextAttributes = [
            NSFontAttributeName:            UIFont(name: "OpenSans-Light", size:17.0)!,
            NSForegroundColorAttributeName: UIColor.blackColor()
        ]
        postsTableView.estimatedRowHeight = 50
        postsTableView.rowHeight = UITableViewAutomaticDimension
        postsTableView.registerNib(postCellNib, forCellReuseIdentifier: "post")
        postsTableView.alpha = 0
        postsTableView.tableFooterView = UIView(frame: CGRectZero)
        postsDownloadingIndicator.startAnimating()

        FBConnection.sharedInstance.getPosts(self)
    }
    
    override func viewDidAppear(animated: Bool) {
        postsDownloadingIndicator.center = postsTableView.center
        loadMoreButton.titleLabel?.text = "load_more".localized
        warningMessage.center = self.view.center
        
        if (self.userid != FBConnection.sharedInstance.getCurrentAccessToken()?.userID) {
            self.userid = FBConnection.sharedInstance.getCurrentAccessToken()?.userID
            viewDidLoad()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayMessage(message: String, title: String, icon: String, color: UIColor) {
        let msg = NSMutableAttributedString(string: "\(icon) \(title)\n" + message, attributes: [
            NSFontAttributeName:            UIFont(name: "OpenSans-Light", size:19.0)!,
            NSForegroundColorAttributeName: color
            ])
        msg.addAttribute(NSFontAttributeName, value: UIFont(name: "FontAwesome", size: 20.0)!, range: NSMakeRange(0, 1))
        
        warningMessage.attributedText = msg
        warningMessage.center.x = self.view.center.x
        warningMessage.center.y = self.view.center.y - 50
        warningMessage.alpha = 0
        
        UIView.animateWithDuration(1.5, delay: 0.2, options: .CurveEaseOut, animations: { () -> Void in
            self.warningMessage.alpha = 1
            self.warningMessage.center.y = self.view.center.y
            }, completion: nil)
    }
    
    // MARK: - Interface events
    
    func tappedAfterError(recognizer: UITapGestureRecognizer) {
        recognizer.view?.removeGestureRecognizer(recognizer)
        FBConnection.sharedInstance.getPosts(self, key: nil)
    }
    
    @IBAction func menuIconTapped(sender: UIBarButtonItem) {
        if (UI_USER_INTERFACE_IDIOM() != .Pad) {
            MenuViewController.menuSlideIn()
        }
    }
    
    @IBAction func loadMoreTapped(sender: AnyObject) {
        postsTableViewOffsetY = postsTableView.contentOffset.y
        FBConnection.sharedInstance.getPosts(self, key: FBPost.nextKey)
    }
    
    // MARK: - UIScrollViewDelegate methods
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if (hasNewPosts) {
            let d = postsTableView.contentSize.height - postsTableView.contentOffset.y
            let control: Int = (UI_USER_INTERFACE_IDIOM() == .Pad) ? 1000 : 800
            
            if(UI_USER_INTERFACE_IDIOM() == .Pad) {
                let loadMoreButton = (self.revealViewController().rearViewController as? MenuViewController)?.loadMore
                
                loadMoreButton?.addTarget(self, action: Selector("loadMoreTapped:"), forControlEvents: .TouchUpInside)
                
                if (d < control.cgfloatValue) {
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        loadMoreButton?.alpha = 1
                    })
                    loadMoreButton?.enabled = true
                } else {
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        loadMoreButton?.alpha = 0
                    })
                     loadMoreButton?.enabled = false
                }
            } else {
                if (d < control.cgfloatValue) {
                    if loadMoreButtonHeight.constant < 60 {
                        self.loadMoreButtonHeight.constant += (400 - d / 2)
                        UIView.animateWithDuration(0.3, animations: { () -> Void in
                            self.postsTableView.layoutIfNeeded()
                        })
                    } else {
                        self.loadMoreButtonHeight.constant = 60
                        self.postsTableView.layoutIfNeeded()
                    }
                } else if (loadMoreButtonHeight.constant > 0) {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.loadMoreButtonHeight.constant = 0
                        UIView.animateWithDuration(0.3, animations: { () -> Void in
                            self.postsTableView.layoutIfNeeded()
                        })
                    })
                }
            }
        } else {
            loadMoreButtonHeight.constant = 0
            postsTableView.layoutIfNeeded()
        }
        
        if (postsTableView.contentOffset.y < -5 && (postsTableViewLastOffsetY > postsTableView.contentOffset.y)) {
            if (refreshProgressBar.alpha == 0) {
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.refreshProgressBar.alpha = 1
                })
            }
            var progress = (isRefreshing) ? 100 : -postsTableView.contentOffset.y / 100
            
            if (progress >= 100) {
                progress = 100
            }
            
            refreshProgressBar.progress = Float(progress)
        } else {
            refreshProgressBar.alpha = 0
        }
        
        postsTableViewLastOffsetY = postsTableView.contentOffset.y
    }
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if (postsTableView.contentOffset.y < -100) {
            fbPosts = [FBPost]()
            FBConnection.sharedInstance.getPosts(self, key: nil)
            isRefreshing = true
            postsTableView.userInteractionEnabled = false
            postsDownloadingIndicator.startAnimating()
            postsDownloadingIndicator.hidden = false
            UIView.animateWithDuration(0.2, animations: { () -> Void in
                self.postsTableView.alpha = 0.2
            })
        }
        
        self.refreshProgressBar.alpha = 0
    }
    
    func scrollViewWillBeginDecelerating(scrollView: UIScrollView) {
        refreshProgressBar.alpha = 0
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.postsCount
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: SinglePostTableViewCell = postsTableView.dequeueReusableCellWithIdentifier("post") as! SinglePostTableViewCell
        var currentPost: FBPost
        if (fbPosts.count > indexPath.row) {
            currentPost = fbPosts[indexPath.row]
        } else {
            currentPost = FBPost()
        }
        
        cell.profileImageDownloadingIndicator.startAnimating()
        
        cell.contentView.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth
        cell.contentView.bounds = CGRectMake(0,0,99999,99999)
        
        cell.userName.text = currentPost.authorName
        if let title = currentPost.name {
                cell.postTitleHeight.constant = 25
                cell.postTitle.text = title
        } else {
            cell.postTitle.text = nil
            cell.postTitleHeight.constant = 0
        }
        if let message = currentPost.message {
            cell.postContentLabel.text = message
        } else if let description = currentPost.desc {
            cell.postContentLabel.text = description
        } else {
            cell.postContentLabel.text = ""
        }
        if let postImage = currentPost.postImage {
            cell.postImageHeight.constant = 150
            cell.postImage.image = postImage
            cell.postImageDownloadingIndicator.alpha = 0
        } else {
            cell.postImage.image = nil
            cell.postImageHeight.constant = 0
            cell.postImageDownloadingIndicator.alpha = 0
        }
        if let profileImage = currentPost.authorProfileImage {
            cell.profileImage.image = profileImage
            if cell.profileImageDownloadingIndicator.alpha == 1 {
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    cell.profileImageDownloadingIndicator.stopAnimating()
                    cell.profileImageDownloadingIndicator.alpha = 0
                    cell.profileImage.alpha = 1
                })
            }
        } else {
            cell.profileImageDownloadingIndicator.alpha = 1
            cell.profileImageDownloadingIndicator.startAnimating()
            cell.profileImage.alpha = 0
        }
        if let progress = currentPost.postImageProgress {
            if (progress < 1) {
                cell.postImageDownloadingIndicator.alpha = 1
                cell.postImageDownloadingIndicator.progress = progress
            }
        }
        cell.creationTime.text = currentPost.friendlyCreateTime
        cell.favoriteIcon.hidden = !currentPost.isFavorite
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (UI_USER_INTERFACE_IDIOM() == .Pad) {
            if let delegate = splitVcDelegate {
                let singlePostVc: SinglePostViewController = delegate.viewControllers[1] as! SinglePostViewController
                
                singlePostVc.updatePostData(self.fbPosts[indexPath.row])
            }
        } else {
            let animation: ((UIViewController, UIViewController, (UIImageView!, CGFloat), () -> Void) -> Void) =
            { (currentlyActiveVc, addedVc, background, completion) -> Void in
                let toVc: SinglePostViewController = addedVc as! SinglePostViewController
                let fromVc: UIViewController = currentlyActiveVc as UIViewController
                let tvRect = tableView.rectForRowAtIndexPath(indexPath)
                let fromSnap = tableView.cellForRowAtIndexPath(indexPath)!.snapshotViewAfterScreenUpdates(true)
                
                fromVc.view.userInteractionEnabled = false
                toVc.view.frame = tableView.convertRect(tvRect, toView: MainContainerViewController.sharedInstance.view)
                toVc.view.alpha = 0
                toVc.updatePostData(self.fbPosts[indexPath.row])
                
                fromSnap.frame = toVc.view.frame
                fromVc.view.addSubview(fromSnap)
                fromSnap.transform = CGAffineTransformMakeScale(1.3, 1.3)
                
                UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                    fromSnap.transform = CGAffineTransformMakeScale(1, 1)
                    }, completion: { (completed) -> Void in
                        UIView.animateWithDuration(0.9, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                            toVc.view.frame = MainContainerViewController.sharedInstance.view.frame
                            toVc.view.alpha = 1
                            }, completion: {(completed) -> Void in
                                fromSnap.removeFromSuperview()
                                completion()
                        })
                })
            }
            
            if let vC = MainContainerViewController.sharedInstance.changeViewTo("SinglePostViewController",
                withAnimation: animation , releaseOld: false, returning: false) {
                    (vC as! SinglePostViewController).returnPoint = "PostsViewController"
            } else {
                let vC = MainContainerViewController.sharedInstance.changeViewTo(SinglePostViewController(nibName: "SinglePostViewController", bundle: nil), named: "SinglePostViewController", withAnimation: animation, releaseOld: false, returning: false)
                (vC as! SinglePostViewController).returnPoint = "PostsViewController"
            }
        }
    }
    
    // MARK: - FBConnectionDelegate methods
    
    func fbDidFetchFeedJSON(feed: AnyObject, nextKey: String?) {
        let jFeed = JSON(feed)

        postsTableView.alpha = 1
        postsDownloadingIndicator.stopAnimating()
        postsDownloadingIndicator.hidden = true
        
        if let key = nextKey {
            FBPost.nextKey = key
        }
        
        postParsing: for (key: String, post: JSON) in jFeed {
            if let p = FBPost(post: post) {
                hasNewPosts = false
                
                for post in fbPosts {
                    if (post.id == p.id) {
                        continue postParsing
                    } else {
                        hasNewPosts = true
                    }
                }
                
                let graphPath = "/\(p.authorId!)/?fields=picture.type(large),cover"
                let imageRequest = FBSDKGraphRequest(graphPath: graphPath, parameters: nil, HTTPMethod: "GET")
                
                imageRequest.startWithCompletionHandler { (fbGraphConnection, result, error) -> Void in
                    if (error == nil) {
                        p.authorProfileImageData = result
                        
                        if let profileImageData: AnyObject = p.authorProfileImageData {
                            let imageData = JSON(profileImageData)
   
                            let isSilhouette = imageData["picture"]["data"]["is_silhouette"].bool
                            let url = imageData["picture"]["data"]["url"].string
                            let coverURL = imageData["cover"]["source"].string
                            
                            if let coverurl = coverURL {
                                Alamofire.request(.GET, coverurl, parameters: nil).response({ (requesst, httpResponse, data, error) -> Void in
                                    if (error == nil) {
                                        if let imageData = data {
                                            let image = UIImage(data: imageData)
                                            p.authorCoverImage = image
                                        } else {
                                            NSLog("Invalid response at cover image.")
                                        }
                                    } else {
                                        NSLog("Failed to download cover image: \(error?.description)")
                                    }
                                })
                            }
                            if let silhouette = isSilhouette {
                                if (!silhouette && url != nil) {
                                    if let image = self.imageCache.objectForKey(url!) as? UIImage {
                                        p.authorProfileImage = image
                                        self.postsTableView.reloadData()
                                    } else {
                                        Alamofire.request(.GET, url!, parameters: nil).validate().response({ (request, httpResponse, data, error) -> Void in
                                            if (error == nil) {
                                                if let imageData = data {
                                                    if let profileImage = UIImage(data: imageData) {
                                                        self.imageCache.setObject(profileImage, forKey: url!)
                                                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                                            p.authorProfileImage = profileImage
                                                            self.postsTableView.reloadData()
                                                        })
                                                        return
                                                    }
                                                }
                                            } else {
                                                NSLog("Profile image download failed with error: \(error?.description)")
                                            }
                                            NSLog("Failed to download profile image. Received data is invalid.")
                                            p.authorProfileImage = UIImage(named: "FBNoImage")
                                        })
                                    }
                                } else {
                                    p.authorProfileImage = UIImage(named: "FBNoImage")
                                }
                            } else {
                                NSLog("Failed to fetch profile image data. Received JSON is not what was expected.")
                                p.authorProfileImage = UIImage(named: "FBNoImage")
                            }
                        }
                    } else {
                        NSLog("An error occured during profile picture data fetch: \(error.description)")
                    }
                }
                
                if let image_url = p.full_picture {
                    if let image = imageCache.objectForKey(image_url) as? UIImage {
                        p.postImage = image
                    } else {
                        Alamofire.request(.GET, image_url, parameters: nil).validate().response({ (request, httpResponse, data, error) -> Void in
                            if (error == nil) {
                                if let imageData = data {
                                    if let image = UIImage(data: imageData) {
                                        self.imageCache.setObject(image, forKey: image_url)
                                        p.postImage = image
                                        self.postsTableView.reloadData()
                                        return
                                    }
                                }
                            }
                            NSLog("An error occured during downloading post image: \(error?.description)")
                        }).progress(closure: { (bytesRead, totalBytesRead, totalBytesExpectedToRead) -> Void in
                            p.postImageProgress = Float(totalBytesRead / totalBytesExpectedToRead)
                        })
                    }
                } else {
                    p.postImage = nil
                    postsTableView.reloadData()
                }
                
                self.fbPosts.append(p)
            }
        }
        
        postsTableView.reloadData()
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.postsTableView.alpha = 1
            self.warningMessage.alpha = 0
        })
        postsTableView.userInteractionEnabled = true
        isRefreshing = false
    }
    
    func fbFetchErrorOccured(error: NSError) {
        switch (error.code) {
            case 0:
                NSLog("No \"data\" key found in fetched JSON.")
            case 1:
                NSLog("Received data is in invalid format.")
            
            default:
                NSLog("An error occured during fetch: \(error.description)")
        }
        
        postsTableView.alpha = 0
        postsDownloadingIndicator.stopAnimating()
        postsDownloadingIndicator.hidden = true
        displayMessage("fetch_error_message".localized, title: "fetch_error_message_title".localized, icon: "\u{f00d}", color: UIColor(red: 0xA9/255, green: 0, blue: 0, alpha: 1))
        
        let tapGr = UITapGestureRecognizer(target: self, action: Selector("tappedAfterError:"))
        
        self.view.addGestureRecognizer(tapGr)
    }
}