//
//  LoginViewController.swift
//  TimelineViewer
//
//  Created by Przemek on 14.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SWRevealViewController


class LoginViewController: UIViewController, FBConnectionDelegate {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var warningMessage: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var loginButtonInitialState = (centerY: 0.cgfloatValue, centerX: 0.cgfloatValue, alpha: 0.cgfloatValue)
    private var containerBackground: (UIImageView!, CGFloat)? = nil
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("iPadRotated:"), name: UIDeviceOrientationDidChangeNotification, object: nil)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let accessToken = FBConnection.sharedInstance.getCurrentAccessToken() {
            fbConnectionAuthenticationSuccessful(nil)
        } else {
            setSubViewsToInitialState()
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            loginButton.bounds = CGRect(x: 0, y: 0, width: 400, height: 70)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad) {
            NSNotificationCenter.defaultCenter().removeObserver(self)
        }
    }
    
    func setSubViewsToInitialState() {
        let loginButtonTitle = NSMutableAttributedString(string: "\u{f09a}   " + "login_with_facebook".localized, attributes: [
            NSFontAttributeName:            UIFont(name: "OpenSans-Semibold", size:(UI_USER_INTERFACE_IDIOM() == .Pad) ? 21 : 13.0)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()
            ])
        loginButtonTitle.addAttribute(NSFontAttributeName, value: UIFont(name: "FontAwesome", size: (UI_USER_INTERFACE_IDIOM() == .Pad) ? 24 : 16.0)!, range: NSMakeRange(0, 1))
        
        loginButtonInitialState.alpha = 1
        loginButtonInitialState.centerX = self.view.center.x
        loginButtonInitialState.centerY = MainContainerViewController.sharedInstance.view.frame.height - 190
        
        loginButton.setAttributedTitle(loginButtonTitle, forState: .Normal)
        loginButton.center.x = loginButtonInitialState.centerX
        loginButton.center.y = loginButtonInitialState.centerY
    }
    
    // MARK: - iPad code
    
    func iPadRotated(notification: NSNotification) {
        setSubViewsToInitialState()
    }
    
    // MARK: - Animations

    func standardEaseInAnimation(containerBackground: (UIImageView!, CGFloat)? = nil, delay: Double = 0, parallax: Bool = false) {
        loginButton.center.x = loginButtonInitialState.centerX
        loginButton.center.y = self.view.bounds.height + 50
        loginButton.alpha = 0
        
        self.view.userInteractionEnabled = false
        
        UIView.animateWithDuration(1.2, delay: delay, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.2, options: .CurveEaseOut, animations: {
            () -> Void in
            self.loginButton.center.y = self.loginButtonInitialState.centerY
            if let cBg = containerBackground {
                if (parallax) {
                    cBg.0.center.y -= cBg.1 / 2
                }
                self.containerBackground = containerBackground
            }
            }, completion: nil)
        UIView.animateWithDuration(1, animations: { () -> Void in
            self.loginButton.alpha = self.loginButtonInitialState.alpha
        }, completion: { (completed) -> Void in
                self.loginButton.transform = CGAffineTransformMakeScale(1.05, 1.05)
                self.view.userInteractionEnabled = true
                
                UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                    self.loginButton.transform = CGAffineTransformMakeScale(1, 1)
                }, completion: nil)
        })
    }
    
    func displayMessage(message: String, title: String, icon: String, color: UIColor) {
        let msg = NSMutableAttributedString(string: "\(icon) \(title)\n" + message, attributes: [
            NSFontAttributeName:            UIFont(name: "OpenSans-Light", size: (UI_USER_INTERFACE_IDIOM() == .Pad) ? 25 : 19.0)!,
            NSForegroundColorAttributeName: color
            ])
        msg.addAttribute(NSFontAttributeName, value: UIFont(name: "FontAwesome", size: (UI_USER_INTERFACE_IDIOM() == .Pad) ? 21 : 26.0)!, range: NSMakeRange(0, 1))
        
        warningMessage.attributedText = msg
        warningMessage.center.x = self.view.center.x
        warningMessage.center.y = 0
        warningMessage.alpha = 0
        
        UIView.animateWithDuration(1.5, delay: 0.2, options: .CurveEaseOut, animations: { () -> Void in
            self.warningMessage.alpha = 1
            self.warningMessage.center.y = 100
        }, completion: nil)
    }
    
    func clearMessage() {
        warningMessage.alpha = 0
        activityIndicator.alpha = 0
        activityIndicator.stopAnimating()
    }
    
    // MARK: - Interface Events
    
    @IBAction func loginButtonTapped(sender: UIButton) {
        sender.transform = CGAffineTransformMakeScale(1.1, 1.1)
        
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
            sender.transform = CGAffineTransformMakeScale(1, 1)
        }, completion: nil)
        UIView.animateWithDuration(0.2, delay: 0.1, options: nil, animations: { () -> Void in
            sender.center.x += self.view.frame.width
            self.activityIndicator.alpha = 1
            self.activityIndicator.startAnimating()
        }) { (completed) -> Void in
            if (completed) {
                FBConnection.sharedInstance.invokeLoginDialog(sender: self)
                self.clearMessage()
                UIView.animateWithDuration(0.2, animations: { () -> Void in
                    self.containerBackground!.0.center.y += self.containerBackground!.1
                })
            }
        }
    }
    
    // MARK: - FBConnectionDelegate methods
    
    func fbConnectionAuthenticationSuccessful(result: FBSDKLoginManagerLoginResult?) {
        let animation: ((UIViewController, UIViewController, (UIImageView!, CGFloat), () -> Void) -> Void) = {
            (currentlyActiveVc, addedVc, background, completion) -> Void in
            let newVc: UIViewController = addedVc
            
            newVc.view.userInteractionEnabled = false
            newVc.view.center.x += MainContainerViewController.sharedInstance.view.frame.width
            
            UIView.animateWithDuration(0.9, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                newVc.view.center.x = MainContainerViewController.sharedInstance.view.center.x
            }, completion: { (completed) -> Void in
                newVc.view.userInteractionEnabled = true
                completion()
            })
            UIView.animateWithDuration(1, animations: { () -> Void in
                background.0.center.x -= background.1 / 2
            })
        }
        
        if (UI_USER_INTERFACE_IDIOM() == .Pad) {
            if let revealController = MainContainerViewController.sharedInstance.changeViewTo("RevealViewController", withAnimation:animation , releaseOld: true) as? SWRevealViewController {
                revealController.rearViewRevealWidth = 53
                revealController.rearViewRevealOverdraw = 173
                revealController.bounceBackOnOverdraw = false
                revealController.stableDragOnOverdraw = true
                revealController.setFrontViewPosition(FrontViewPosition.Right, animated: true)
                revealController.delegate = MainContainerViewController.sharedInstance
                
                var frontViewFrame = revealController.frontViewController.view.bounds
                let xOrigin = revealController.view.bounds.origin.x
                let nWidth = revealController.view.bounds.size.width - 53
                
                frontViewFrame.origin.x = xOrigin
                frontViewFrame.size.width = nWidth
                
                revealController.frontViewController.view.frame = frontViewFrame
            } else {
                let splitController = MainSplitViewController()
                let postsController = PostsViewController(nibName: "PostsViewController", bundle: nil)
                
                postsController.splitVcDelegate = splitController
                
                splitController.viewControllers = [
                    postsController,
                    SinglePostViewController(nibName: "SinglePostViewController", bundle: nil)
                ]
                
                let menuViewController = MenuViewController(nibName: "MenuViewController", bundle: nil)
                let revealController = SWRevealViewController(rearViewController: menuViewController, frontViewController: splitController)
                
                revealController.rearViewRevealWidth = 53
                revealController.rearViewRevealOverdraw = 173
                revealController.bounceBackOnOverdraw = false
                revealController.stableDragOnOverdraw = true
                revealController.setFrontViewPosition(FrontViewPosition.Right, animated: true)
                revealController.delegate = MainContainerViewController.sharedInstance
                
                var frontViewFrame = revealController.frontViewController.view.bounds
                let xOrigin = revealController.view.bounds.origin.x
                let nWidth = revealController.view.bounds.size.width - 53
                
                frontViewFrame.origin.x = xOrigin
                frontViewFrame.size.width = nWidth
                
                revealController.frontViewController.view.frame = frontViewFrame
                
                MainContainerViewController.sharedInstance.changeViewTo(revealController, named: "RevealViewController", withAnimation:animation , releaseOld: true)
            }
        } else {
            if let menuViewController: MenuViewController =
                MainContainerViewController.sharedInstance.changeViewTo("MenuViewController", withAnimation:animation , releaseOld: true) as? MenuViewController {
                    menuViewController.menuShowChoosedItem(.Posts, withAnimation: .AppLaunching)
            } else {
                let menuViewController: MenuViewController = MainContainerViewController.sharedInstance.changeViewTo(MenuViewController(nibName: "MenuViewController", bundle: nil), named: "MenuViewController", withAnimation:animation , releaseOld: true) as! MenuViewController
                menuViewController.menuShowChoosedItem(.Posts, withAnimation: .AppLaunching)
            }
        }
    }
    
    func fbConnectionAuthenticationCancelled() {
        standardEaseInAnimation(containerBackground: self.containerBackground, delay: 0.5)
        displayMessage("facebook_cancelled_message".localized, title: "facebook_cancelled_message_title".localized, icon: "\u{f127}", color: UIColor.lightGrayColor())
    }
    
    func fbConnectionAuthenticationError(error: NSError) {
        standardEaseInAnimation()
        displayMessage("facebook_error_message".localized, title: "facebook_error_message".localized, icon: "\u{f00d}", color: UIColor(red: 0xA9/255, green: 0, blue: 0, alpha: 1))
    }
}