//
//  AccountViewController.swift
//  TimelineViewer
//
//  Created by Przemek on 23.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class AccountViewController: UIViewController {

    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let profile = FBSDKProfile.currentProfile() {
            profileName.text = profile.name
        }
        
        if (UI_USER_INTERFACE_IDIOM() == .Pad) {
            menuButton.alpha = 0
            menuButton.enabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Inteface events
    
    @IBAction func menuIconTapped(sender: AnyObject) {
        MenuViewController.menuSlideIn()
    }
}