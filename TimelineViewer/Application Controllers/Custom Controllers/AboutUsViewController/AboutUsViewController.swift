//
//  AboutUsViewController.swift
//  TimelineViewer
//
//  Created by Przemek on 23.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet weak var menuButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (UI_USER_INTERFACE_IDIOM() == .Pad) {
            menuButton.hidden = true
            menuButton.enabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Interface events
    
    @IBAction func test(sender: AnyObject) {
        MenuViewController.menuSlideIn()
    }
}