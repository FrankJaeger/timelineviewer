//
//  SinglePostViewController.swift
//  TimelineViewer
//
//  Created by Przemek on 27.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import UIKit

class SinglePostViewController: UIViewController {
    
    @IBOutlet weak var postScrollView: UIScrollView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var creationTime: UILabel!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postMessage: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postDescription: UILabel!
    @IBOutlet weak var postCaption: UILabel!
    @IBOutlet weak var postImageHeight: NSLayoutConstraint!
    @IBOutlet weak var addToFavoritesButton: UIButton!
    
    var returnPoint = "PostsViewController"
    
    private var currentPost: FBPost? = nil
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        postScrollView.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        postMessage.font = UIFont(name: "OpenSans", size: 12)
        addToFavoritesButton.titleLabel?.text = "add_to_favorites".localized
        
        if (currentPost == nil) {
            self.view.subviews.map({($0 as! UIView).hidden = true})
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updatePostData(post: FBPost) {
        userName.text = post.authorName!
        profileImage.image = post.authorProfileImage
        creationTime.text = post.friendlyCreateTime
        postTitle.text = post.name
        postMessage.text = post.message
        postDescription.text = post.desc
        postCaption.text = post.caption
        if let postImage = post.postImage {
            self.postImage.image = postImage
            postImageHeight.constant = (UI_USER_INTERFACE_IDIOM() == .Pad) ? 300 : 150
        } else {
            postImageHeight.constant = 0
        }
        if let coverImage = post.authorCoverImage {
            self.coverImage.image = coverImage
        } else {
            self.coverImage.image = UIImage(named: "placeholder.gif")
        }
        if (post.isFavorite) {
            makeFavButtonGreen()
        } else {
            makeFavButtonBlue()
        }
        
        self.currentPost = post
        self.view.subviews.map({($0 as! UIView).hidden = false})
    }
    
    func makeFavButtonGreen() {
        addToFavoritesButton.backgroundColor = UIColor(red: 0x5a/255, green: 0xbd/255, blue: 0x1e/255, alpha: 1)
        addToFavoritesButton.userInteractionEnabled = false
        addToFavoritesButton.setTitle("already_in_favorites".localized, forState: UIControlState.Normal)
        addToFavoritesButton.setImage(UIImage(named: "FavoriteWhite"), forState: UIControlState.Normal)
    }
    
    func makeFavButtonBlue() {
        addToFavoritesButton.backgroundColor = UIColor(red: 0x44/255, green: 0x60/255, blue: 0x9d/255, alpha: 1)
        addToFavoritesButton.userInteractionEnabled = true
        addToFavoritesButton.setTitle("add_to_favorites".localized, forState: UIControlState.Normal)
        addToFavoritesButton.setImage(UIImage(named: "PlusIcon"), forState: UIControlState.Normal)
    }
    
    // MARK: - Interface events
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        let anim: ((UIViewController, UIViewController, (UIImageView!, CGFloat), () -> Void) -> Void) =
        { (currentlyActiveVc, addedVc, background, completion) -> Void in
            addedVc.view.userInteractionEnabled = true
            completion()
        }
        
        self.view.transform = CGAffineTransformMakeScale(1.2, 1.2)
        UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
            self.view.transform = CGAffineTransformMakeScale(1, 1)
        }, completion: {
            (completed) -> Void in
            if (MainContainerViewController.sharedInstance.changeViewTo(self.returnPoint,
                    withAnimation: anim, releaseOld: true, returning: false) == nil) {
                        let vC = (self.returnPoint == "PostsViewController") ? PostsViewController(nibName: "PostsViewController", bundle: nil) :
                            FavoritesViewController(nibName: "FavoritesViewController", bundle: nil)
                    MainContainerViewController.sharedInstance.changeViewTo(vC, named: "PostsViewController", withAnimation: anim, releaseOld: true, returning: false)
            }
        })
        UIView.animateWithDuration(0.7, delay: 0.6, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
            self.view.center.y += MainContainerViewController.sharedInstance.view.bounds.height
            self.view.transform = CGAffineTransformMakeScale(0.4, 0.4)
            self.view.alpha = 0
        }, completion: nil)   
    }
    @IBAction func addToFavoritesTapped(sender: UIButton) {
        if let post = currentPost {
            if (FBPostStore.savePost(post)) {
                makeFavButtonGreen()
            }
        }
        sender.transform = CGAffineTransformMakeScale(1.3, 1.3)
        UIView.animateWithDuration(0.6, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
            sender.transform = CGAffineTransformMakeScale(1, 1)
        }, completion: nil)
    }
}