//
//  FavoritesViewController.swift
//  TimelineViewer
//
//  Created by Przemek on 23.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var postsTableView: UITableView!
    @IBOutlet weak var postsDownloadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var warningMessage: UILabel!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var splitVcDelegate: UISplitViewController? = nil
    
    private var favPosts = FBPostStore.getFavPosts()
    private var postsCount: Int {
        if (favPosts == nil) {
            return 0
        } else {
            return favPosts!.count
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UI_USER_INTERFACE_IDIOM() == .Pad) {
            menuButton.image = nil
            menuButton.enabled = false
        }

        let postCellNib = UINib(nibName: "SinglePostTableViewCell", bundle: nil)
        
        navBar.topItem?.title = "favorites".localized
        navBar.titleTextAttributes = [
            NSFontAttributeName:            UIFont(name: "OpenSans-Light", size:17.0)!,
            NSForegroundColorAttributeName: UIColor.blackColor()
        ]
        postsTableView.estimatedRowHeight = 50
        postsTableView.rowHeight = UITableViewAutomaticDimension
        postsTableView.registerNib(postCellNib, forCellReuseIdentifier: "post")
        postsTableView.alpha = 0
        postsTableView.tableFooterView = UIView(frame: CGRectZero)
        postsDownloadingIndicator.startAnimating()
    }
    
    override func viewWillAppear(animated: Bool) {
        favPosts = FBPostStore.getFavPosts()

        if (favPosts != nil && favPosts?.count > 0) {
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.postsTableView.alpha = 1
            })
        } else {
            displayMessage("no_favorites_message".localized, title: "no_favorites_message_title".localized, icon: "\u{f097}", color: UIColor.lightGrayColor())
            self.postsTableView.alpha = 0
        }
        
        postsDownloadingIndicator.stopAnimating()
        postsDownloadingIndicator.hidden = true
        favPosts = FBPostStore.getFavPosts()
        postsTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayMessage(message: String, title: String, icon: String, color: UIColor) {
        let msg = NSMutableAttributedString(string: "\(icon) \(title)\n" + message, attributes: [
            NSFontAttributeName:            UIFont(name: "OpenSans-Light", size:19.0)!,
            NSForegroundColorAttributeName: color
            ])
        msg.addAttribute(NSFontAttributeName, value: UIFont(name: "FontAwesome", size: 20.0)!, range: NSMakeRange(0, 1))
        
        warningMessage.attributedText = msg
        warningMessage.center.x = self.view.center.x
        warningMessage.center.y = self.view.center.y - 50
        warningMessage.alpha = 0
        
        UIView.animateWithDuration(1.5, delay: 0.2, options: .CurveEaseOut, animations: { () -> Void in
            self.warningMessage.alpha = 1
            self.warningMessage.center.y = self.view.center.y
            }, completion: nil)
    }
    
    // MARK: - Interface events
    
    @IBAction func test(sender: AnyObject) {
        MenuViewController.menuSlideIn()
    }
    
    @IBAction func menuButtonTapped(sender: AnyObject) {
        MenuViewController.menuSlideIn()
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.postsCount
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: SinglePostTableViewCell = postsTableView.dequeueReusableCellWithIdentifier("post") as! SinglePostTableViewCell
        
        if let fPosts = favPosts {
            let currentPost = fPosts[indexPath.row]
            
            cell.profileImageDownloadingIndicator.startAnimating()
            
            cell.contentView.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth
            cell.contentView.bounds = CGRectMake(0,0,99999,99999)
            
            cell.userName.text = currentPost.authorName
            if let title = currentPost.name {
                cell.postTitleHeight.constant = 25
                cell.postTitle.text = title
            } else {
                cell.postTitle.text = nil
                cell.postTitleHeight.constant = 0
            }
            if let message = currentPost.message {
                cell.postContentLabel.text = message
            } else if let description = currentPost.desc {
                cell.postContentLabel.text = description
            } else {
                cell.postContentLabel.text = ""
            }
            if let postImage = currentPost.postImage {
                cell.postImageHeight.constant = 150
                cell.postImage.image = postImage
                cell.postImageDownloadingIndicator.alpha = 0
            } else {
                cell.postImage.image = nil
                cell.postImageHeight.constant = 0
                cell.postImageDownloadingIndicator.alpha = 0
            }
            if let profileImage = currentPost.authorProfileImage {
                cell.profileImage.image = profileImage
                if cell.profileImageDownloadingIndicator.alpha == 1 {
                    UIView.animateWithDuration(0.3, animations: { () -> Void in
                        cell.profileImageDownloadingIndicator.stopAnimating()
                        cell.profileImageDownloadingIndicator.alpha = 0
                        cell.profileImage.alpha = 1
                    })
                }
            } else {
                cell.profileImageDownloadingIndicator.alpha = 1
                cell.profileImageDownloadingIndicator.startAnimating()
                cell.profileImage.alpha = 0
            }
            if let progress = currentPost.postImageProgress {
                if (progress < 1) {
                    cell.postImageDownloadingIndicator.alpha = 1
                    cell.postImageDownloadingIndicator.progress = progress
                }
            }
            cell.creationTime.text = currentPost.friendlyCreateTime
            cell.favoriteIcon.hidden = !currentPost.isFavorite
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let posts = favPosts {
            if (UI_USER_INTERFACE_IDIOM() == .Pad) {
                if let delegate = splitVcDelegate {
                    let singlePostVc: SinglePostViewController = delegate.viewControllers[1] as! SinglePostViewController
                    
                    singlePostVc.updatePostData(self.favPosts![indexPath.row])
                }
            } else {
                let animation: ((UIViewController, UIViewController, (UIImageView!, CGFloat), () -> Void) -> Void) =
                { (currentlyActiveVc, addedVc, background, completion) -> Void in
                    let toVc: SinglePostViewController = addedVc as! SinglePostViewController
                    let fromVc: UIViewController = currentlyActiveVc as UIViewController
                    let tvRect = tableView.rectForRowAtIndexPath(indexPath)
                    let fromSnap = tableView.cellForRowAtIndexPath(indexPath)!.snapshotViewAfterScreenUpdates(true)
                    
                    fromVc.view.userInteractionEnabled = false
                    toVc.view.frame = tableView.convertRect(tvRect, toView: MainContainerViewController.sharedInstance.view)
                    toVc.view.alpha = 0
                    toVc.updatePostData(self.favPosts![indexPath.row])
                    
                    fromSnap.frame = toVc.view.frame
                    fromVc.view.addSubview(fromSnap)
                    fromSnap.transform = CGAffineTransformMakeScale(1.3, 1.3)
                    
                    UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                        fromSnap.transform = CGAffineTransformMakeScale(1, 1)
                        }, completion: { (completed) -> Void in
                            UIView.animateWithDuration(0.9, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: nil, animations: { () -> Void in
                                toVc.view.frame = MainContainerViewController.sharedInstance.view.frame
                                toVc.view.alpha = 1
                                }, completion: {(completed) -> Void in
                                    fromSnap.removeFromSuperview()
                                    completion()
                            })
                    })
                }
                
                if let vC = MainContainerViewController.sharedInstance.changeViewTo("SinglePostViewController",
                    withAnimation: animation , releaseOld: false, returning: false) {
                        (vC as! SinglePostViewController).returnPoint = "FavoritesViewController"
                } else {
                    let vC = MainContainerViewController.sharedInstance.changeViewTo(SinglePostViewController(nibName: "SinglePostViewController", bundle: nil), named: "SinglePostViewController", withAnimation: animation, releaseOld: false, returning: false)
                    (vC as! SinglePostViewController).returnPoint = "FavoritesViewController"
                }

            }
        }
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            if let posts = favPosts {
                let post = posts[indexPath.row]
                
                FBPostStore.removePost(post.id)
                favPosts?.removeAtIndex(indexPath.row)
                postsTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            }
        }
        
        viewWillAppear(true)
    }
}