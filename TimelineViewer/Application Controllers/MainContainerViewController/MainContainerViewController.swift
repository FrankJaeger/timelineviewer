//
//  MainContainerViewController.swift
//  TimelineViewer
//
//  Created by Przemek on 18.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import UIKit
import SWRevealViewController

class MainContainerViewController: UIViewController, SWRevealViewControllerDelegate {
    
    static let sharedInstance = MainContainerViewController(nibName: "MainContainerViewController", bundle: nil)
    
    private let backgroundOffset: CGFloat = 50.0
    private let vcStack = ViewControllersStack(workAsOrderedDictionary: true)
    
    @IBOutlet weak var containerBackground: UIImageView!
    
    internal var activeViewController: Int = 0
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
    }
    
    override private init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("facebookTokenDidChange:"), name: FBConnection.AccessTokenDidChangeNotification, object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let animation: ((UIViewController, UIViewController, (UIImageView!, CGFloat), () -> Void) -> Void) = {
            (currentlyActiveVc, addedVc, background, completion) -> Void in
            let fromVc: LoginViewController = addedVc as! LoginViewController
            
            fromVc.standardEaseInAnimation(containerBackground: background, parallax: true)
        }
        if (changeViewTo("LoginViewController", withAnimation:animation , releaseOld: false) == nil) {
            changeViewTo(LoginViewController(nibName: "LoginViewController", bundle: nil), named: "LoginViewController",
            withAnimation:animation , releaseOld: false)
        }
        containerBackground.bounds = CGRect(x: 0, y: 0,
            width:  self.view.bounds.width + backgroundOffset,
            height: self.view.bounds.height + backgroundOffset)
    }
    
    override func viewDidLayoutSubviews() {
        self.containerBackground.center = self.view.center
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func changeViewTo<T>(viewController: T, named name: String? = nil, withAnimation animation: (
            currentlyActiveVc: UIViewController,
            addedVc: UIViewController,
            background: (image: UIImageView!, offset: CGFloat),
            completion: ()->()) -> Void,
            releaseOld: Bool,
            returning: Bool? = false) -> UIViewController? {
        assert(viewController is String || viewController is UIViewController, "viewController must be of type String or UIViewController!")
        
        let oldVcIndex = self.activeViewController
        var newVcIndex = 0
        var vcName = ""

        if (viewController is UIViewController) {
            if let nm = name {
                newVcIndex = vcStack.updateViewController(viewController as! UIViewController, named: nm)
                vcName = nm
            } else {
                (newVcIndex, vcName) = vcStack.push(viewController as! UIViewController)
            }
            //NSLog("ViewController: \(vcName) created.")
        } else {
            let vcName = viewController as! String
            
            if let idx = vcStack.getIndexOfViewControllerNamed(viewController as! String) {
                newVcIndex = idx
            } else {
                //NSLog("No viewController named \(vcName) found.")
                return nil
            }
        }

        self.activeViewController = newVcIndex
                
        if (returning != nil && !returning!) {
            vcStack[newVcIndex]!.view.frame = self.view.frame
        }
                
        self.addChildViewController(vcStack[newVcIndex]!)
        self.view.addSubview(vcStack[newVcIndex]!.view)
                
        animation(currentlyActiveVc: vcStack[oldVcIndex]!, addedVc: vcStack[newVcIndex]!, background: (containerBackground, backgroundOffset)) {
            if (releaseOld) {
                self.containerBackground.subviews.map({$0.removeFromSuperview()})   // Removing blur
                self.vcStack[oldVcIndex]!.view.removeFromSuperview()
                self.vcStack[oldVcIndex]!.removeFromParentViewController()
                self.vcStack[oldVcIndex]!.view = nil
            }
        }
                
        return vcStack[newVcIndex]
    }
    
    func animateView(name: String, withAnimation animation: (viewController: UIViewController?, background: (image: UIImageView!, offset: CGFloat)) -> Void) {
        animation(viewController: vcStack[name], background: (image: containerBackground, offset: backgroundOffset))
    }
    
    func facebookTokenDidChange(notification: NSNotification) {
        let info = notification.userInfo as! Dictionary<String, AnyObject>
        
        if (info["FBSDKAccessToken"] == nil) {
            for i in 0..<vcStack.count {
                var blur: UIView? = nil
                
                if (self.containerBackground.subviews.count > 0) {
                    blur = self.containerBackground.subviews[0] as? UIView
                }
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    UIView.animateWithDuration(0.5, animations: { () -> Void in
                        self.vcStack[i]?.view.alpha = 0
                        blur?.alpha = 0
                        }, completion: { (completed) -> Void in
                            self.vcStack[i]?.view.removeFromSuperview()
                            self.vcStack[i]?.removeFromParentViewController()
                            self.vcStack[i]?.view.alpha = 1
                            self.vcStack[i]?.view = nil
                            blur?.removeFromSuperview()
                    })
                })
                
                if (i == self.vcStack.count - 1) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), { () -> Void in
                        self.viewDidAppear(true)
                    })
                }
            }
        }
    }
    
    // MARK: - SWRevealViewControllerDelegate methods
    
    func revealController(revealController: SWRevealViewController!, didAddViewController viewController: UIViewController!, forOperation operation: SWRevealControllerOperation, animated: Bool) {
        var frontViewFrame = revealController.frontViewController.view.bounds
        let xOrigin = revealController.view.bounds.origin.x
        let nWidth = revealController.view.bounds.size.width - 53
        
        frontViewFrame.origin.x = xOrigin
        frontViewFrame.size.width = nWidth
        revealController.frontViewController.view.frame = frontViewFrame
    }
}