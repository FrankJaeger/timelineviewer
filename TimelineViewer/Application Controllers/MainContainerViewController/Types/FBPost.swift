//
//  FBPost.swift
//  TimelineViewer
//
//  Created by Przemek on 26.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import Foundation
import SwiftyJSON

class FBPost: NSObject {
    
    static var nextKey = ""
    
    var from: AnyObject = ""
    var name: String? = nil
    var id: String = ""
    var full_picture: String? = nil
    var link: String? = nil
    var type: String? = nil
    var created_time: String = ""
    var desc: String? = nil
    var message: String? = nil
    var source: String? = nil
    var caption: String? = nil
    var attachments: AnyObject? = nil
    
    var authorProfileImageData: AnyObject? = nil
    var authorProfileImage: UIImage? = nil
    var authorCoverImage: UIImage? = nil
    var postImage: UIImage? = nil
    var postImageProgress: Float? = nil
    var storedAuthorName: String? = nil
    var storedAuthorId: String? = nil
    var storedFriendlyCreatedTime: String? = nil
    
    var authorName: String? {
        let data = JSON(from)
        
        if let name = data["name"].string {
            return name
        } else {
            return storedAuthorName
        }
    }
    var authorId: String? {
        let data = JSON(from)
        if let id = data["id"].string {
            return id
        } else {
            return storedAuthorId
        }
    }
    var friendlyCreateTime: String? {
        if FBPost.makeRFC3339friendly(created_time) == "date_error".localized {
            return storedFriendlyCreatedTime
        } else {
            return FBPost.makeRFC3339friendly(created_time)
        }
    }
    var isFavorite: Bool {
        let favPosts = FBPostStore.getFavPosts()
        var isFavorite = false
        
        if (favPosts != nil) {
            for favpost in favPosts! {
                if (self.id == favpost.id) {
                    isFavorite = true
                }
            }
        }
        return isFavorite
    }
    
    override init() {
        super.init()
    }
    
    convenience init?(post: JSON) {
        self.init()
        if (post["from"] != nil && post["id"] != nil) {
            for (key: String, subJson: JSON) in post {
                if (self.respondsToSelector(Selector(key)) && key != "description") {
                    self.setValue(subJson.object, forKey: key)
                }
                if (key == "description") {
                    self.desc = subJson.string
                }
            }
            
            if let attachment_type = post["attachments"]["data"][0]["type"].string {
                if (attachment_type == "note") {
                    self.desc = post["attachments"]["data"][0]["description"].string
                    self.name = post["attachments"]["data"][0]["title"].string
                }
            }
            
            if (name == nil) {
                name = post["attachments"]["data"][0]["title"].string
            }
            if (full_picture == nil) {
                full_picture = post["attachments"]["data"][0]["media"]["image"]["src"].string
            }
        } else {
            NSLog("Invalid post format.")
            return nil
        }
    }
    
    static func makeRFC3339friendly(rfcTime: String) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        
        let date = dateFormatter.dateFromString(rfcTime)
        
        if let unwrappedDate = date {
            let timeSinceDate: NSTimeInterval = NSDate().timeIntervalSinceDate(unwrappedDate)
            
            if (timeSinceDate < 24 * 3600) {
                let hoursSinceDate = timeSinceDate / 3600
                
                switch (Int(hoursSinceDate)) {
                case 0:
                    let minutesSinceDate = timeSinceDate / 60
                    if (minutesSinceDate < 3) {
                        return "just_now".localized
                    }
                    return "\(Int(minutesSinceDate)) " + "min_ago".localized
                    
                default:
                    return "\(Int(hoursSinceDate))" + "h_ago".localized
                }
            } else {
                var createTime = ""
                
                dateFormatter.dateFormat = "dd MMMM"
                createTime += dateFormatter.stringFromDate(unwrappedDate)
                dateFormatter.dateFormat = "HH:mm"
                createTime += " " + "at".localized + " "
                createTime += dateFormatter.stringFromDate(unwrappedDate)
                
                return createTime
            }
        }
        
        return "date_error".localized
    }
}