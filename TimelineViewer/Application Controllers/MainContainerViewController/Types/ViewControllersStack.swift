//
//  ViewControllersStack.swift
//  TimelineViewer
//
//  Created by Przemek on 19.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import Foundation
import UIKit

class ViewControllersStack<T where T: UIViewController> {
    
    internal var workAsOrderedDictionary = false
    internal var isEmpty: Bool {
        return vcIndexes.count == 0
    }
    internal var count: Int {
        return vcIndexes.count
    }
    
    private var viewControllers = [String: T]()
    private var vcIndexes = [String]()
    
    init(workAsOrderedDictionary: Bool) {
        self.workAsOrderedDictionary = workAsOrderedDictionary
    }
    
    func appendViewController(viewController: T, named name: String) {
        viewControllers.updateValue(viewController, forKey: name)
        vcIndexes.append(name)
    }
    
    func updateViewController(viewController: T, named name: String) -> Int {
        if (find(vcIndexes, name) != nil) {
            viewControllers[name] = viewController
        } else {
            appendViewController(viewController, named: name)
        }
        
        return find(vcIndexes, name)!
    }
    
    func updateViewController(viewController: T, atIndex idx: Int) -> String {
        assert(vcIndexes.count > idx, "Index out of bounds! Nothing to update.")
        
        viewControllers.updateValue(viewController, forKey: vcIndexes[idx])
        
        return vcIndexes[idx]
    }

    func removeViewControllerNamed(name: String) -> T {
        let removedVc: T? = viewControllers.removeValueForKey(name)
        
        assert(removedVc != nil, "No ViewController found for name: \(name). There's nothing to remove.")
        
        let idx = find(vcIndexes, name)
        vcIndexes.removeAtIndex(idx!)
        
        return removedVc!
    }
    
    func removeViewControllerAtIndex(idx: Int) -> T {
        assert(vcIndexes.count > idx, "No ViewController at index: \(idx) (out of bounds). There's nothing to remove.")
        
        vcIndexes.removeAtIndex(idx)
        return viewControllers.removeValueForKey(vcIndexes[idx])!
    }

    func getViewControllerNamed(name: String) -> T? {
        if (!workAsOrderedDictionary) {
            if let idx = find(vcIndexes, name) {
                vcIndexes.removeAtIndex(idx)
                vcIndexes.append(name)
            }
        }
        
        return viewControllers[name]
    }
    
    func getViewControllerAtIndex(idx: Int) -> T? {
        if (vcIndexes.count > idx) {
            if (!workAsOrderedDictionary) {
                let tempName = vcIndexes[idx]
            
                vcIndexes.removeAtIndex(idx)
                vcIndexes.append(tempName)
            }
            
            return viewControllers[vcIndexes[idx]]
        } else {
            return nil
        }
    }
    
    func getTop() -> T? {
        if (!isEmpty) {
            return getViewControllerAtIndex(vcIndexes.count - 1)
        } else {
            return nil
        }
    }
    
    func getIndexOfViewControllerNamed(name: String) -> Int? {
        return find(vcIndexes, name)
    }
    
    func push(viewController: T) -> (index: Int, name: String) {
        let idx = vcIndexes.count
        let name = "viewController#\(idx)"
        appendViewController(viewController, named: name)
        
        return (idx, name)    }
    
    func pop() -> T? {
        if (!isEmpty) {
            return removeViewControllerAtIndex(vcIndexes.count - 1)
        } else {
            return nil
        }
    }
    
    subscript(nameOrIndex: AnyObject) -> T? {
        get {
            assert(nameOrIndex is String || nameOrIndex is Int, "Incompatible subscript type! Only String or Int are allowed.")
            
            if (nameOrIndex is String) {
                return getViewControllerNamed(nameOrIndex as! String)
            } else {
                return getViewControllerAtIndex(nameOrIndex as! Int)
            }
        }
        
        set {
            assert(nameOrIndex is String || nameOrIndex is Int, "Incompatible subscript type! Only String or Int are allowed.")
            
            if (nameOrIndex is String) {
                updateViewController(newValue!, named: nameOrIndex as! String)
            } else {
                updateViewController(newValue!, atIndex: nameOrIndex as! Int)
            }
        }
    }
}