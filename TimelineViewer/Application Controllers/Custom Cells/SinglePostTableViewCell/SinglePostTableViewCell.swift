//
//  SinglePostTableViewCell.swift
//  TimelineViewer
//
//  Created by Przemek on 26.07.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Alamofire

class SinglePostTableViewCell: UITableViewCell {

    @IBOutlet weak var postContentLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var creationTime: UILabel!
    @IBOutlet weak var favoriteIcon: UIImageView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var profileImageDownloadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var postImageDownloadingIndicator: UIProgressView!
    @IBOutlet weak var postTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var postImageHeight: NSLayoutConstraint!
    
    var request: Alamofire.Request? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        postTitle.font = UIFont(name: "OpenSans", size: 15)
        postContentLabel.font = UIFont(name: "OpenSans-Light", size: 15)
        userName.font = UIFont(name: "OpenSans", size: 16)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}